export const state = () => ({
  user: null,
  credit: 0
});

export const mutations = {
  login(state, user) {
    state.user = user;
  },
  removeUser(state) {
    state.user = null;
  },
  updateCredit(state, credit) {
    state.credit = credit;
  },
  increaseCredit(state) {
    state.credit += 5;
  }
};

export const actions = {
  logout({ commit }) {
    this.$axios.get('/api/logout').then(() => {
      commit('removeUser');
    });
  },
  async nuxtServerInit({ commit }) {
    const user = await this.$axios.$get('/api/user');
    if (user) {
      commit('login', user);
      const { credit } = await this.$axios.$get('/api/profile');
      commit('updateCredit', credit);
    }
  }
};
