module.exports = {
  apps: [
    {
      name: 'parlare-frontend',
      script: 'npm',
      args: 'run start',
      env: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production',
        PORT: 3005,
        API_SERVER: 'http://localhost:3006'
      }
    }
  ],
  deploy: {
    production: {
      user: 'root',
      host: 'maoyachen.com',
      ref: 'origin/master',
      repo: 'git@bitbucket.org:clysto/parlare-frontend.git',
      path: '/var/app/parlare-frontend',
      'post-deploy':
        'npm install && npm run build && pm2 startOrRestart ecosystem.config.js --env production'
    }
  }
};
