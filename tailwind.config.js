/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    extend: {
      fontSize: {
        avatar8: '1rem',
        avatar12: '1.25rem',
        avatar16: '1.875rem'
      }
    },
    screens: {
      sm: { max: '639px' },
      md: { max: '767px' },
      lg: { max: '1023px' },
      xl: { max: '1279px' }
    },
    maxHeight: {
      '0': '0',
      '1/2': '50vh',
      full: '100%'
    },
    colors: {
      mask: 'rgba(0,0,0,0.5)',
      black: '#000000',
      white: '#ffffff',
      danger: '#aa0000',
      blue: '#003ee6',
      yellow: {
        light: '#fffdc4',
        dark: '#fefa87'
      },
      gray: {
        '100': '#f5f5f5',
        '200': '#eeeeee',
        '300': '#e0e0e0',
        '400': '#bdbdbd',
        '500': '#9e9e9e',
        '600': '#757575',
        '700': '#616161',
        '800': '#424242',
        '900': '#212121'
      }
    }
  },
  variants: {
    borderWidth: ['first', 'last'],
    margin: ['first', 'last'],
    backgroundColor: ['responsive', 'hover', 'focus', 'active']
  },
  plugins: []
};
